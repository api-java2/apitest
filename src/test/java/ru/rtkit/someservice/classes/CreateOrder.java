package ru.rtkit.someservice.classes;

public class CreateOrder {

    private Integer petId;
    private Integer id;
    private Integer quantity;
    private String shipDate;
    private String status;
    private Boolean complete;

    public CreateOrder() {}

    public Integer getPetId() {
        return petId;
    }

    public CreateOrder setPetId(int petId) {
        this.petId = petId;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public CreateOrder setId(int orderId) {
        this.id = orderId;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public CreateOrder setQuantity(int quantity) {
        this.quantity = quantity;
        return this;
    }

    public String getShipDate() {
        return shipDate;
    }

    public CreateOrder setShipDate(String shipDate) {
        this.shipDate = shipDate;
        return this;
    }

    public String getStatus() {
        return status;
    }

    public CreateOrder setStatus(String status) {
        this.status = status;
        return this;
    }

    public Boolean getComplete() {
        return complete;
    }

    public CreateOrder setComplete(boolean complete) {
        this.complete = complete;
        return this;
    }

    public static CreateOrder getOrderInfo(){
        return new CreateOrder()
                .setId(101)
                .setPetId(100)
                .setQuantity(1)
                .setShipDate("2022-04-11T04:23:19.390Z")
                .setStatus("placed")
                .setComplete(true);
    }
}
