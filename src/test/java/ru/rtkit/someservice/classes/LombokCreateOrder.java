package ru.rtkit.someservice.classes;

import lombok.Data;

@Data
public class LombokCreateOrder {

    private Integer petId;
    private Integer id;
    private Integer quantity;
    private String shipDate;
    private String status;
    private Boolean complete;

}
