package ru.rtkit.someservice.apiHelpers;

public class Endpoints {

    public static final String ADD_PET = "/pet";
    public static final String PET_BY_ID = "/pet/{petId}";
    public static final String CREATE_ORDER = "/store/order";
    public static final String ORDER_BY_ID = "/store/order/{orderId}";
}
