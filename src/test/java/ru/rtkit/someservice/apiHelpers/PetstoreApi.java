package ru.rtkit.someservice.apiHelpers;

import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.ResponseSpecification;
import ru.rtkit.someservice.classes.CreateOrder;
import ru.rtkit.someservice.classes.LombokCreateOrder;

import static io.restassured.RestAssured.given;
import static ru.rtkit.someservice.tests.BaseTest.props;

public class PetstoreApi {

    public PetstoreApi(){
        RestAssured.baseURI = props.getProperty("URI");
        RestAssured.defaultParser = Parser.JSON;
        RestAssured.filters(new AllureRestAssured());
    }

    @Step("Получить по id {endpoint}")
    public Response get(String endpoint, ResponseSpecification resp, Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .get(endpoint, pathParams);
    }

    @Step("Разместить по id {endpoint}")
    public Response post(String endpoint, Object body, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(body)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

    @Step("Удалить по id {endpoint}")
    public Response delete(String endpoint, ResponseSpecification resp, Object... pathParams) {
        return given()
                .contentType(ContentType.JSON)
                .expect()
                .spec(resp)
                .when()
                .delete(endpoint, pathParams);
    }

    @Step("Разместить заказ по id {endpoint}")
    public Response postOrder(String endpoint, CreateOrder order, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(order)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

    @Step("Разместить заказ по id {endpoint}")
    public Response postOrderLombok(String endpoint, LombokCreateOrder lombokOrder, ResponseSpecification resp) {
        return given()
                .contentType(ContentType.JSON)
                .body(lombokOrder)
                .expect()
                .spec(resp)
                .when()
                .post(endpoint);
    }

}
