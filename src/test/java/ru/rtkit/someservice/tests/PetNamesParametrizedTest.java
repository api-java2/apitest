package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.TmsLink;
import org.hamcrest.Matchers;
import org.json.JSONObject;
import ru.rtkit.someservice.apiHelpers.Endpoints;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@DisplayName("Параметризованные тесты питомцев")
public class PetNamesParametrizedTest extends BaseTest {

    @ParameterizedTest
    @DisplayName("Проверить валидацию имён при создании питомцев")
    @Description("Тест на проверку валидации имён питомцев")
    @TmsLink("1")
    @ValueSource(
            strings = {
            "Busya",
            "BUSYA",
            "busya",
            "BuSyA",
            "Буся",
            "БУСЯ",
            "буся",
            "БуСя"
            }
    )

    public void createPetsWithDifferentNamesTest(String name) {
        JSONObject body = new JSONObject()
                .put("name", name);

        long petId = api.post(Endpoints.ADD_PET, body.toString(), response200)
                    .jsonPath()
                    .getLong("id");

        api.get(Endpoints.PET_BY_ID, response200, petId)
                .then()
                .assertThat()
                .body("name", Matchers.equalTo(name));
    }
}
