package ru.rtkit.someservice.tests;

import ru.rtkit.someservice.apiHelpers.PetstoreApi;
import io.restassured.specification.ResponseSpecification;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static io.restassured.RestAssured.expect;

public class BaseTest {

    public static ResponseSpecification response200 = expect().statusCode(200);

    public static PetstoreApi api;
    public static Properties props;

    static {
        String service = "service";
        InputStream is = ClassLoader.getSystemResourceAsStream( service + ".properties");
        props = new Properties();
        try {
            props.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }

        api = new PetstoreApi();
    }

}
