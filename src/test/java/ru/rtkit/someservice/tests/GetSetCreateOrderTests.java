package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.TmsLink;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.someservice.classes.CreateOrder;
import ru.rtkit.someservice.apiHelpers.Endpoints;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.oneOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static ru.rtkit.someservice.tests.BaseTest.api;
import static ru.rtkit.someservice.tests.BaseTest.response200;

public class GetSetCreateOrderTests {

    private CreateOrder createOrder;

    @Test
    @DisplayName("Проверить, что создался заказ с корректными параметрами с помощью геттеров/сеттеров")
    @Description("Тест на успешное создание заказа")
    @TmsLink("5")
    public void successfulOrderCreationTest(){
        createOrder = CreateOrder.getOrderInfo();
        Response response = api.postOrder(Endpoints.CREATE_ORDER, createOrder, response200)
                .then()
                .extract().response();

        int actId = response.path("petId");
        String actStatus = response.path("status");
        boolean actComplete = response.path("complete");
        String actShipDate = response.path("shipDate");

        assertEquals(createOrder.getPetId(), actId, "Некорректный id питомца");
        assertEquals(actStatus, createOrder.getStatus(), "Некорректный статус питомца");
        assertEquals(createOrder.getComplete(), actComplete, "Некорректный статус завершения оформления заказа");
        assertNotNull(actShipDate, "Поле даты отгрузки пустое");
    }

}
