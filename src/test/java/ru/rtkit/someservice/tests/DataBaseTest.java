package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.TmsLink;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBaseTest {

    String pathToDB = getClass().getClassLoader().getResource("sqlite/chinook.db").getPath();
    String dbUrl = "jdbc:sqlite:" + pathToDB;

    @Test
    @DisplayName("Вывод в консоль названия трека по его id")
    @Description("Тест на вывод названия трека в консоль")
    @TmsLink("3")
    public void getTrackNameByIdTest(){
        String query = "SELECT * FROM Tracks WHERE TrackId=1";
        List<String> tracks = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                tracks.add(rs.getString("name"));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        System.out.println(tracks);
    }

    @Test
    @DisplayName("Вывод в консоль всех записей с жанром Pop")
    @Description("Тест на вывод записей с опредленным жанром")
    @TmsLink("4")
    public void getAllPopGenreFromTrackTest(){
        String query = "SELECT * FROM tracks INNER JOIN genres ON genres.genreId = tracks.genreId WHERE genres.name = 'Pop'";
        try (Connection conn = DriverManager.getConnection(dbUrl);
             PreparedStatement ps = conn.prepareStatement(query);
             ResultSet rs = ps.executeQuery()){
            int columns = rs.getMetaData().getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columns; i++){
                    System.out.print(rs.getString(i) + "\n");
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
