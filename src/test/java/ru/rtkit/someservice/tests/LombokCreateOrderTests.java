package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.TmsLink;
import io.restassured.response.Response;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.someservice.classes.LombokCreateOrder;
import ru.rtkit.someservice.apiHelpers.Endpoints;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.oneOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static ru.rtkit.someservice.tests.BaseTest.api;
import static ru.rtkit.someservice.tests.BaseTest.response200;

public class LombokCreateOrderTests {

    @Test
    @DisplayName("Проверить, что создался заказ с корректными параметрами с помощью lombok")
    @Description("Тест на успешное создание заказа")
    @TmsLink("6")
    public void successfulOrderCreationTest(){
        LombokCreateOrder lombokCreateOrder = new LombokCreateOrder();
        lombokCreateOrder.setId(101);
        lombokCreateOrder.setPetId(100);
        lombokCreateOrder.setQuantity(1);
        lombokCreateOrder.setShipDate("2022-04-11T04:23:19.390Z");
        lombokCreateOrder.setStatus("placed");
        lombokCreateOrder.setComplete(true);

        Response response = api.postOrderLombok(Endpoints.CREATE_ORDER, lombokCreateOrder, response200)
                .then()
                .extract().response();

        int actId = response.path("petId");
        String actStatus = response.path("status");
        boolean actComplete = response.path("complete");
        String actShipDate = response.path("shipDate");

        assertEquals(lombokCreateOrder.getPetId(), actId, "Некорректный id питомца");
        assertEquals(actStatus, lombokCreateOrder.getStatus(), "Некорректный статус питомца");
        assertEquals(lombokCreateOrder.getComplete(), actComplete, "Некорректный статус завершения оформления заказа");
        assertNotNull(actShipDate, "Поле даты отгрузки пустое");
    }

}
