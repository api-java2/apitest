package ru.rtkit.someservice.tests;

import io.qameta.allure.Description;
import io.qameta.allure.TmsLink;
import io.restassured.response.Response;
import org.json.JSONObject;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.rtkit.someservice.apiHelpers.Endpoints;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Тесты создания заказа")
public class CreateOrderTest extends BaseTest {

    @BeforeAll
    @DisplayName("Создать питомца перед созданием заказа")
    public static void beforeAll(){
        JSONObject bodyPet = new JSONObject()
                .put("id", 100)
                .put("name", "Ктулху")
                .put("status", "available");

        api.post(Endpoints.ADD_PET, bodyPet.toString(), response200);
    }

    @AfterAll
    @DisplayName("Удалить питомца и заказ после прохождения теста на создание заказа")
    public static void afterAll(){
        api.delete(Endpoints.PET_BY_ID, response200, 100);
        api.delete(Endpoints.ORDER_BY_ID, response200, 101);
    }

    @Test
    @DisplayName("Проверить, что создался заказ с корректными параметрами")
    @Description("Тест на успешное создание заказа")
    @TmsLink("2")
    public void successfulOrderCreationTest(){

        JSONObject body = new JSONObject()
                .put("id", 101)
                .put("petId", 100)
                .put("quantity", 1)
                .put("shipDate", "2022-04-11T04:23:19.390Z")
                .put("status", "placed")
                .put("complete", true);

        Response response = api.post(Endpoints.CREATE_ORDER, body.toString(), response200)
                .then()
                .extract().response();

        int actId = response.path("petId");
        String actStatus = response.path("status");
        boolean actComplete = response.path("complete");
        String actShipDate = response.path("shipDate");

        assertEquals(100, actId, "Некорректный id питомца");
        assertThat(actStatus, oneOf("placed", "approved", "delivered"));
        assertEquals(true, actComplete, "Некорректный статус завершения оформления заказа");
        assertNotNull(actShipDate, "Поле даты отгрузки пустое");
    }

}
