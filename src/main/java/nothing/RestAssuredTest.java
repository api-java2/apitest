/*
package nothing;

import io.restassured.http.ContentType;
import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static io.restassured.RestAssured.given;

public class RestAssuredTest {

    public static Logger LOGGER = LoggerFactory.getLogger(RestAssuredTest.class);

    @Test
    public void giveTheCatInGoodHandsTest() {

        String id = RandomStringUtils.randomNumeric(2, 3);
        LOGGER.error("Error message");

        JSONObject bodyForPostCat = new JSONObject()
                .put("id", id)
                .put("name", "Ктулху")
                .put("status", "available");

        RestAssured.given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyForPostCat.toString())
                .post("https://petstore.swagger.io/v2/pet")
                .then()
                .log().ifError()
                .statusCode(200);

        JSONObject bodyForUpdateCat = new JSONObject()
                .put("id", id)
                .put("name", "Муся")
                .put("status", "available");

        RestAssured.given()
                .when()
                .contentType(ContentType.JSON)
                .body(bodyForUpdateCat.toString())
                .put("https://petstore.swagger.io/v2/pet")
                .then()
                .log().ifError()
                .statusCode(200);

        RestAssured.given()
                .when()
                .pathParam("petId", id)
                .get("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().ifError()
                .statusCode(200);

        RestAssured.given()
                .when()
                .pathParam("petId", id)
                .delete("https://petstore.swagger.io/v2/pet/{petId}")
                .then()
                .log().ifError()
                .statusCode(200);
    }

*/
//}